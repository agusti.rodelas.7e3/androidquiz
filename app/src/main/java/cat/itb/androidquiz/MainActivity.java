package cat.itb.androidquiz;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    TextView questionText, progressText;
    Button falseOption, trueOption;
    ProgressBar progressBar;
    AlertDialog.Builder builder;
    int cont = 0;
    boolean answer;

    String[] questionStrings = new String[10];
    ArrayList<QuestionModel> questions = new ArrayList<QuestionModel>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        questionText = findViewById(R.id.textViewQuestion);
        progressText = findViewById(R.id.textProgress);
        falseOption = findViewById(R.id.buttonFalse);
        trueOption = findViewById(R.id.buttonTrue);
        questionStrings = getResources().getStringArray(R.array.Questions);
        progressBar = findViewById(R.id.progressBar);

        builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.title_alert_dialog);
        builder.setMessage(R.string.missatge_alert_dialog);
        builder.setNegativeButton(R.string.finish_alert_dialog, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        builder.setPositiveButton(R.string.restart_alert_dialog, null);


        questions.add(new QuestionModel(0, true));
        questions.add(new QuestionModel(1,false));
        questions.add(new QuestionModel(2, false));
        questions.add(new QuestionModel(3, false));
        questions.add(new QuestionModel(4, true));
        questions.add(new QuestionModel(5, false));
        questions.add(new QuestionModel(6, true));
        questions.add(new QuestionModel(7, false));
        questions.add(new QuestionModel(8, true));
        questions.add( new QuestionModel(9, false));

         questionText.setText(questionStrings[cont]);
         progressText.setText("Question "+(cont+1)+" of "+questionStrings.length);
         progressBar.setMax(questionStrings.length);
         progressBar.setProgress(1);
         //Toast.makeText(this, Arrays.toString(questionStrings), Toast.LENGTH_SHORT).show();
         falseOption.setOnClickListener(this);
         trueOption.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonTrue:
                answer = true;
                break;
            case R.id.buttonFalse:
                answer = false;
                break;
        }

        if(questions.get(cont).getAnswer() == answer){
            Toast.makeText(this, "Correct", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this, "not Correct", Toast.LENGTH_SHORT).show();
        }

        if(cont < questionStrings.length - 1){
            cont++;
        }else{
            AlertDialog dialog = builder.create();
            dialog.show();
            cont = 0;
        }

        questionText.setText(questionStrings[cont]);
        progressBarController(cont);
        progressText.setText("Question "+(cont+1)+" of "+questionStrings.length);
    }

    private void progressBarController(int progress) {
        if(progress < questionStrings.length) {
            progressBar.setProgress(progress+1);
        }else {
            progressBar.setProgress(0);
        }
    }
}
package cat.itb.androidquiz;

public class QuestionModel {

    //Attributes
    int idQuestion;
    boolean answer;

    //Constructor
    public QuestionModel(int idQuestion, boolean answer) {
        this.idQuestion = idQuestion;
        this.answer = answer;
    }

    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    public boolean getAnswer() {
        return answer;
    }

    public void setAnswer(boolean answer) {
        this.answer = answer;
    }
}
